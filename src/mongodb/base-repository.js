const { Pagination } = require('../../paginators')
const { ObjectId } = require('./connection')

class BaseRepository {
  constructor (name, schema, connector) {
		this.name = name
		this.schema = schema
    this.connector = connector
  }

  convertToObjectId (id) { return new ObjectId(id) }

  async create (body) {
		try {
			const connection = await this.connector.createConnection()
			const model = connection.model(this.name, this.schema)
			const created = await model.create(body)
			return created
		} 
		catch(error) {
			console.log(error)
			await this.closeConnection()
			throw error
		} 
  }

  async paginate (params) {
		try {
			const connection = await this.connector.createConnection()
			const model = connection.model(this.name, this.schema)
			const data = await new Pagination(model, params).getReturn()
			return data
		}
		catch(error) {
			console.log(error)
			await this.closeConnection()
			throw error
		} 
  }

  async find (params) {
		try {
			const connection = await this.connector.createConnection()
			const model = connection.model(this.name, this.schema)
			let { select, limit, offset, sort, lean , query } = params

			lean = lean || false

			const data = await model.find(query)
				.select(select)
				.limit(limit)
				.skip(offset)
				.sort(sort)
				.lean(lean)

			return data
		}
		catch(error) {
			console.log(error)
			await this.closeConnection()
			throw error
		} 
  }

  async findOne (select) {
    try {
			const connection = await this.connector.createConnection()
			const model = connection.model(this.name, this.schema)
			const data = await model.findOne(select)
			return data
		}
		catch(error) {
			console.log(error)
			await this.closeConnection()
			throw error
		} 
  }

  async findById (_id) {
    try {
			_id = await this.convertToObjectId(_id)
			const connection = await this.connector.createConnection()
			const model = connection.model(this.name, this.schema)
			const object = await model.findById(_id)
			return object
		} 
		catch(error) {
			console.log(error)
			await this.closeConnection()
			throw error
		} 
  }

  async delete (_id, forceDelete = false) {
		try {
			if(!forceDelete) {
				const result = await this.update({ _id }, { deletedAt: new Date() })
				return result
			}
			const connection = await this.connector.createConnection()
			const model = connection.model(this.name, this.schema)
			
			const result = await model.remove({ _id })
			return result
		}
		catch(error) {
			console.log(error)
			await this.closeConnection()
			throw error
		} 
  }

  async save (modelObject) {
		try {
			if (!modelObject.updatedAt) modelObject.updatedAt = null
			modelObject.updatedAt = new Date()
    	await modelObject.save()
		}
		catch(error) {
			console.log(error)
			await this.closeConnection()
			throw error
		} 
  }

  async aggregate (params) {
    try {
			const connection = await this.connector.createConnection()
			const model = connection.model(this.name, this.schema)
			const data = await model.aggregate(params)
			return data
		}
		catch(error) {
			console.log(error)
			await this.closeConnection()
			throw error
		} 
  }

  async update (conditions, body) {
    try {
			const connection = await this.connector.createConnection()
			const model = connection.model(this.name, this.schema)
			body.updatedAt = new Date()
			const data = await model.update(conditions, body)
			return data
		} 
		catch(error) {
			console.log(error)
			await this.closeConnection()
			throw error
		} 
	}
	
	async getModel () {
		try {
			const connection = await this.connector.createConnection()
			const model = connection.model(this.name, this.schema)
			return model
		}
		catch(error) {
			console.log(error)
			await this.closeConnection()
			throw error
		} 
	}
	async closeConnection () {
		await this.connector.closeConnection()
	}
}

module.exports.BaseRepository = BaseRepository
