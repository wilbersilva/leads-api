let mongoose = null

try {
	mongoose = require('mongoose')
} catch (error) {
	console.log(error)
	throw new Error('You need install mongoose dependence')
}

const connections = {}

class MongoConnection {

	constructor(url, unique = true) {
		this.url = url
		this.unique = unique
	}

	async createConnection() {
		
		const options = { 
			useNewUrlParser: true,
			// useUnifiedTopology: true 
		}
		
		if (connections[this.url]) {
			return connections[this.url]
		}

		if (this.unique) {
			const connection = await mongoose.connect(this.url, options)
			this.connection = connection
			connections[this.url] = this.connection
			return this.connection
		}

		const connection = await mongoose.createConnection(this.url, options)

		this.connection = connection
		connections[this.url] = this.connection
		return this.connection
	}

	async closeConnection() {
		if (this.unique) {
			await mongoose.connection.close()
			return
		}
		await this.connection.close()
	}
}

module.exports.MongoConnection = MongoConnection
module.exports.ObjectId = mongoose.Types.ObjectId