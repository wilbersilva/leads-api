const { MongoConnection, ObjectId } = require('./connection');
const { BaseRepository } = require('./base-repository')

module.exports.MongoConnection = MongoConnection
module.exports.ObjectId = ObjectId
module.exports.BaseRepository = BaseRepository