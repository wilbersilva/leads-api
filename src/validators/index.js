const { isValid : CPF } = require('./cpf')
const { isValid: CNPJ } = require('./cnpj')
const { validationJoiSchema } = require('./joi')

module.exports.CPF = CPF
module.exports.CNPJ = CNPJ
module.exports.JoiValidation = validationJoiSchema