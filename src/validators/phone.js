
module.exports = {
  acceptyOnlyPhoneOrCell (findValue) {
    const phoneRegex = /^\(\d{2}\) \d{4,5}-\d{4}$/gi
    return !phoneRegex.test(findValue)
  },
  validatedType (apparatus, typeApparatus, changeRule) {
    const conditionCell = apparatus.length === 15
    const typeValidated = conditionCell ? 'cell' : 'phone'
    let forceError = true
    if (typeValidated === typeApparatus) forceError = false
    if (changeRule) forceError = false // Ipremi data company with one phone only
    return forceError
  },
  getDDD (number) { // (11) xxxx-xxxx
    const ddd = number.slice(0, 4)
    return ddd
  },
  getNumber (number) {
    const numberApparatus = number.slice(5)
    return numberApparatus
  },
  dddOtherType (number) { // (11)
    const ddd = number.slice(1, 3)
    return ddd
  },
  numberOtherType (number) { // xxxxx-xxxxx
    const numberApparatus = number.replace('-', '')
    return numberApparatus
  }
}
