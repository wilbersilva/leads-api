let Joi

try {
	Joi = require('@hapi/joi')
} catch (error) {
	console.log('You need install Joi dependence')
}

module.exports.validationJoiSchema = (schema, body) => {
	const { error } = schema.validate(body, { abortEarly: false })
	if (error) 
		return {
			isValid: false,
			message: error.message,
			errors: error.details
		}
	
	return { isValid: true }
}