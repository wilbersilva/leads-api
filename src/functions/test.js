const { ResponseSuccess } = require('./../http')

module.exports.handler = async () => new ResponseSuccess({ test: 'ok' })
